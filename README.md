To build:

  * Install Java

  * Install Ant

  * Either:

      * Edit this line in build.xml to point your copy of ImageJ:

            <pathelement location="C:\\Program Files\\ImageJ\\ij.jar"/>

      * Or add `ij.jar` to your CLASSPATH environment variable.

  * Then run:

        ant

Alternatively, an Eclipse project file is included which should allow
you to build inside Eclipse.
